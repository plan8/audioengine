declare const _default: {
    scenes: {
        main: {
            sounds: ({
                name: string;
                sound: {
                    "91": string;
                };
                bus?: undefined;
                loop?: undefined;
                gain?: undefined;
                spatial?: undefined;
            } | {
                name: string;
                sound: string;
                bus: string;
                loop: boolean;
                gain: number;
                spatial: {
                    positioned: boolean;
                    static: boolean;
                    refDistance: number;
                    rolloffFactor: number;
                };
            })[];
        };
        otherScene: {
            sounds: {
                name: string;
                sound: string;
            }[];
        };
    };
    sources: ({
        name: string;
        file: string;
        bus: string;
        gain: number;
        loop: boolean;
        spatial: {
            positioned: boolean;
            static: boolean;
        };
    } | {
        name: string;
        file: string;
        bus: string;
        loop: boolean;
        gain?: undefined;
        spatial?: undefined;
    })[];
    busses: {
        name: string;
        parent: string;
    }[];
};
export default _default;
