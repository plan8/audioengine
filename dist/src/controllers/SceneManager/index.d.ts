import { IInstrument } from '../../instruments';
import { IVector3 } from '../../utils';
import { IBusSettings } from '../../routing/Bus';
import { ISourceSettings } from '../BufferManager';
export declare type InstrumentType = 'player' | 'sampler';
export interface ISoundSettings extends ISourceSettings {
    sound: string;
    type?: InstrumentType;
    root?: number;
    loop?: boolean;
    loopStart?: number;
    loopEnd?: number;
}
export interface IConfig {
    scenes: {
        [key: string]: {
            sounds: Array<ISoundSettings>;
        };
    };
    sources: Array<ISourceSettings>;
    busses: Array<IBusSettings>;
}
export default class {
    private players;
    private scenes;
    private masterGain;
    private soundSources;
    private busses;
    private bufferManager;
    private currentScene;
    private listener;
    audioIsInitialized: boolean;
    masterContext: AudioContext;
    utils: {
        calculateDistance: (vec1: import("../../utils").IVector2, vec2: import("../../utils").IVector2) => number;
        getRangeBasedOnDistance: (settings: {
            listenerPosition: IVector3;
            position: IVector3;
            maxDistance: number;
            refDistance: number;
        }) => number;
        waveshapers: {
            bottomOut: (bottomBalance?: number) => import("tone").WaveShaper;
        };
        clip: (value: number, min: number, max?: number) => number;
        getContext: () => import("tone").BaseContext;
        setMuteOnBlur: (busToMute: string, fadeTime: number, engineInstance: any) => void;
        isWebkit: (UA: string) => boolean;
        isMobile: (UA: string) => RegExpMatchArray;
    };
    loadingState: 'initializing' | 'loading' | 'loaded';
    constructor(config: IConfig);
    private setupAudioGraph;
    initAudio(): Promise<void>;
    destroy(): void;
    loadScene(sceneName: string, setCurrent?: boolean, onLoaded?: any, fadeTime?: number): Promise<void>;
    private getPlayerKeysForScene;
    changeScene(sceneName: string, settings: {
        fadeOutTime?: number;
        fadeInTime?: number;
        onLoaded?: any;
    }): false | Promise<void>;
    private unloadSounds;
    unloadScene(sceneName: string): void;
    private createSoundPlayer;
    getBus(key: string): any;
    getInstrument(key: string): IInstrument;
    getInstrumentForScene(key: string): IInstrument;
    trigger(key: string, options?: any): void;
    triggerForScene(key: string, options?: any): boolean;
    triggerAllForScene(scene: string, options?: any): void;
    stopForScene(key: string, options?: any): void;
    stop(key: string, options?: {
        fadeOut?: number;
        startTime?: number;
        onComplete?: any;
    }): void;
    private stopPlayer;
    fadeOut(key: string, options?: {
        fadeOut?: number;
        startTime?: number;
        onComplete?: any;
    }): void;
    fadeOutPlayer(instrument: IInstrument, options?: {
        fadeOut?: number;
        startTime?: number;
        onComplete?: any;
    }): void;
    setPositionForScene(key: string, position: IVector3): boolean;
    setPosition(key: string, position: IVector3): boolean;
    setListenerPosition(playerPosition: IVector3, playerRotation: IVector3): boolean;
    setBusVolume(key: string, volume: number, fadeTime?: number, startTime?: number): void;
    setBusMute(key: string, shouldMute: boolean): any;
    setBaseFilePath(path: string): void;
    now(): number;
}
