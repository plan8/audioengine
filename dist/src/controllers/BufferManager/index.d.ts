import { ToneAudioBuffer } from 'tone';
export interface ISourceSettings {
    name: string;
    file: string;
    bus?: string;
    loop?: boolean;
    gain?: number;
    sends?: [
        {
            bus: string;
            gain: number;
            pre: boolean;
        }
    ];
    spatial?: {
        positioned?: boolean;
        static?: boolean;
        refDistance?: number;
        rolloffFactor?: number;
    };
}
export default class {
    private sources;
    private buffers;
    private baseFilePath;
    constructor(sources: ISourceSettings[], baseFilePath: string);
    getSoundBuffer(name: string): Promise<ToneAudioBuffer | AudioBuffer>;
    setBaseFilePath(path: string): void;
}
