export declare class Channel {
    private panner;
    private gain;
    private initialGain;
    private isStatic;
    private pannerPosition;
    private gainSignal;
    private sends;
    private node;
    maxAudibleDistance: number;
    isPositional: boolean;
    constructor(node: any, settings?: any);
    private setupAudioGraph;
    add(node: any): void;
    addSend(bus: any, options?: any): void;
    connect(destination: any): void;
    setPosition(x: number, y?: number, z?: number): void;
    getPosition(): {
        x: number;
        y: number;
        z: number;
    };
    resetPosition(): void;
    setGain(gain: number, fadeTime?: number, startTime?: number): void;
    setSendGain(busName: string, gain: number, fadeTime?: number, startTime?: number): void;
    resetGain(fadeTime?: number): void;
    fadeIn(fadeTime: number, when?: number): void;
    fadeOut(fadeTime: number, when?: number): void;
    dispose(): void;
}
export default Channel;
