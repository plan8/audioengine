export interface IBusSettings {
    name: string;
    parent: string;
    gain?: number;
    effects?: any[];
}
export declare class Bus {
    private input;
    private output;
    private muteGain;
    private effectInstances;
    private lastEffect;
    private isMuted;
    private resolveSources;
    settings: any;
    constructor(settings: IBusSettings, resolveSources: () => Promise<any>);
    private setupAudioGraph;
    private setupEffects;
    private createEffect;
    connect(destination: any): void;
    setVolume(volume: number, fadeTime?: number, startTime?: number): void;
    setMuteState(shouldMute: boolean): Promise<unknown>;
    dispose(): void;
}
export default Bus;
