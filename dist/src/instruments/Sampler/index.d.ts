import { Sampler, ToneAudioBuffer } from 'tone';
import { Channel } from '../../routing/Channel';
import { ISoundSettings } from '../../controllers/SceneManager';
import { IInstrument } from '../../instruments';
export default class implements IInstrument {
    player: Sampler;
    channel: Channel;
    name: string;
    constructor(buffers: {
        [note: number]: ToneAudioBuffer | AudioBuffer;
    } | {}, options: ISoundSettings);
    setGain(value: number, fadeTime?: number, startTime?: number): void;
    connect(destination: any): void;
    getState(): string;
    private getActiveSources;
    getNumberOfActiveSources(): number;
    stopOldestNote(): void;
    play({ noReset, options }: {
        noReset?: boolean;
        options: any;
    }): void;
    stop(): void;
    fadeOut(fadeTime: any, startTime: any, onComplete: any): void;
    dispose(): void;
}
