export interface IInstrument {
    player: any;
    channel: any;
    name: string;
    setGain: (value: number, fadeTime: number, startTime: number) => void;
    connect: (destination: any) => void;
    getState: () => string;
    play: (options: {
        noReset: boolean;
        when: number;
        fadeIn: number;
        options: any;
    }) => void;
    stop: (when: number) => void;
    fadeOut: (fadeTime: number, startTime: number, onComplete: any) => void;
    dispose: () => void;
}
