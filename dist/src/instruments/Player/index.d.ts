import { Player, ToneAudioBuffer } from 'tone';
import { Channel } from '../../routing/Channel';
import { ISoundSettings } from '../../controllers/SceneManager';
import { IInstrument } from '../../instruments';
export default class implements IInstrument {
    player: Player;
    channel: Channel;
    name: string;
    constructor(buffer: ToneAudioBuffer | AudioBuffer, options: ISoundSettings);
    setGain(value: number, fadeTime?: number, startTime?: number): void;
    connect(destination: any): void;
    getState(): import("tone").BasicPlaybackState;
    play({ noReset, when, fadeIn }: {
        noReset?: boolean;
        when?: number;
        fadeIn?: number;
    }): void;
    stop(when: number): void;
    fadeOut(fadeTime: any, startTime: any, onComplete: any): void;
    dispose(): void;
}
