export interface IBusSettings {
    name: string;
    parent: string;
    gain?: number;
    effects?: any[];
}
export declare class Bus {
    private output;
    constructor(settings: any);
    connect(node: any): void;
}
export default Bus;
