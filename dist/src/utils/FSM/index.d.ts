declare const _default: (stateMachineDefinition: any) => {
    value: any;
    transition(currentState: any, event: any): any;
};
export default _default;
