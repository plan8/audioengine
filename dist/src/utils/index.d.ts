import { WaveShaper } from 'tone';
export interface IVector2 {
    x: number;
    y: number;
}
export interface IVector3 {
    x: number;
    y: number;
    z: number;
}
export declare const calculateDistance: (vec1: IVector2, vec2: IVector2) => number;
export declare const clip: (value: number, min: number, max?: number) => number;
export declare const getRangeBasedOnDistance: (settings: {
    listenerPosition: IVector3;
    position: IVector3;
    maxDistance: number;
    refDistance: number;
}) => number;
export declare const waveshapers: {
    bottomOut: (bottomBalance?: number) => WaveShaper;
};
export declare const getContext: () => import("tone").BaseContext;
export declare const setMuteOnBlur: (busToMute: string, fadeTime: number, engineInstance: any) => void;
export declare const isWebkit: (UA: string) => boolean;
export declare const isMobile: (UA: string) => RegExpMatchArray;
declare const _default: {
    calculateDistance: (vec1: IVector2, vec2: IVector2) => number;
    getRangeBasedOnDistance: (settings: {
        listenerPosition: IVector3;
        position: IVector3;
        maxDistance: number;
        refDistance: number;
    }) => number;
    waveshapers: {
        bottomOut: (bottomBalance?: number) => WaveShaper;
    };
    clip: (value: number, min: number, max?: number) => number;
    getContext: () => import("tone").BaseContext;
    setMuteOnBlur: (busToMute: string, fadeTime: number, engineInstance: any) => void;
    isWebkit: (UA: string) => boolean;
    isMobile: (UA: string) => RegExpMatchArray;
};
export default _default;
