export declare const getFPS: (now: number, previous: number) => number;
export declare const getNormalizedForFPS: (value: number, fps: number) => number;
declare const _default: {
    getFPS: (now: number, previous: number) => number;
    getNormalizedForFPS: (value: number, fps: number) => number;
};
export default _default;
