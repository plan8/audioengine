import{ Gain, LFO, now } from 'tone';
import { waveshapers } from '../../utils';

export interface IBusSettings {
  name: string;
  parent: string;
  gain?: number;
  effects?: any[];
}

export class Bus {
  private output: Gain;

  constructor(settings: any) {
    const { frequency, depth, waveshaper } = settings;

    const vol = new Gain();
    const lfo = new LFO(frequency, 1 - depth, 1); // hertz, min, max
    if (waveshaper) {
      const { sparseness } = waveshaper;
      const bottomOutShaper = waveshapers.bottomOut(sparseness);

      lfo.connect(bottomOutShaper);
      bottomOutShaper.connect(vol.gain);
    }
    else {
      lfo.connect(vol.gain);
    }

    lfo.start();

    this.output = vol;
  }

  public connect(node: any) {
    this.output.connect(node);
  }
};

export default Bus;
