import { Panner3D, Panner, Gain, Signal, now } from 'tone';

const defaultSettings = {
  gain: 1,
  spatial: {
    isPositional: false,
    refDistance: 9,
    rolloffFactor: 5,
  }
};

export class Channel {
  private panner: Panner3D | Panner;
  private gain: Gain;
  private initialGain: number;
  private isStatic: boolean = false;
  private pannerPosition: [number, number, number] | null = null;
  private gainSignal: Signal;
  private sends: object = {};
  private node: any;
  
  public maxAudibleDistance: number = 40;
  public isPositional: boolean = false;

  constructor(node: any, settings?: any) {
    settings = {...defaultSettings, ...settings};
    settings.spatial = {...defaultSettings.spatial, ...settings.spatial};

    const {
      spatial,
      gain,
    } = settings;
    this.initialGain = gain !== undefined ? gain : 1;

    this.isStatic = spatial?.static;
    this.isPositional = spatial?.positioned;
    this.maxAudibleDistance = spatial?.maxAudibleDistance || 40;
    this.setupAudioGraph(this.isPositional, settings);

    this.gainSignal = new Signal({
      value: this.initialGain,
    }).connect(this.gain.gain);

    if (node) {
      this.add(node);
    }
    

  }

  private setupAudioGraph(isPositional: boolean = false, settings: any) {
    const { spatial: { refDistance, rolloffFactor } } = settings;
    this.gain = new Gain(0);

    let panner;
    if (isPositional) {
      panner = this.panner = new Panner3D({
        panningModel: 'equalpower',
        refDistance,
        rolloffFactor,
      });

    }
    else {
      panner = this.panner = new Panner();
    }

    panner.connect(this.gain);
  }

  public add(node: any) {
    if (this.isPositional) {
      node.connect(this.panner);
    }
    else {
      node.connect(this.gain);
    }
    this.node = node;
  }

  public addSend(bus: any, options?: any) {
    const { gain = 0 } = options;
  
    const sendGain = new Gain();
    sendGain.gain.value = gain;
  
    if (options.pre) {
      this.node.connect(sendGain); // connects the output of the channel to the bus
    }
    else {
      this.gain.connect(sendGain); // connects the output of the channel to the bus
    }
  
    sendGain.connect(bus.input);
  
    this.sends[bus.settings.name] = sendGain;
  }

  public connect(destination: any) {
    this.gain.connect(destination);
  }

  public setPosition(x: number, y?: number, z?: number) {
    if (this.isPositional && this.isStatic && !this.pannerPosition) {
      (this.panner as Panner3D).setPosition(x, y, z);
      this.pannerPosition = [x, y, z];
    }
    else if(this.isPositional && !this.isStatic) {
      (this.panner as Panner3D).setPosition(x, y, z);
      this.pannerPosition = [x, y, z];
    }
    else {
      // not a positional element
      this.panner.set({ pan: x });
      this.pannerPosition = [x, 0, 0]; // discard y and z
    }
  }

  public getPosition() {
    if (this.isPositional) {
      const panner = this.panner as Panner3D;
      return { x: panner.positionX.value, y: panner.positionY.value, z: panner.positionZ.value };
    }
    else {
      const panner = this.panner as Panner;
      return { x: panner.pan.value, y: 0, z: 0 };
    }
  }

  public resetPosition() {
    this.pannerPosition = null;
  }

  public setGain(gain: number, fadeTime?: number, startTime?: number) {
    fadeTime = fadeTime || 0;
    startTime = startTime || 0;
    this.gainSignal.cancelAndHoldAtTime(now());
    this.gainSignal.rampTo(gain, fadeTime, now() + startTime);
  }

  public setSendGain(busName: string, gain: number, fadeTime?: number, startTime?: number) {
    fadeTime = fadeTime || 0;
    startTime = startTime || 0;
    const bus = this.sends[busName];

    if (bus) {
      bus.gain.cancelAndHoldAtTime(now());
      bus.gain.rampTo(gain, fadeTime, now() + startTime);
    }
  }

  public resetGain(fadeTime?: number) {
    fadeTime = fadeTime || 0;
    this.gainSignal.cancelAndHoldAtTime(now());
    this.gainSignal.rampTo(this.initialGain, fadeTime, now());
  }

  public fadeIn(fadeTime: number, when?: number) {
    this.gainSignal.cancelAndHoldAtTime(now());
    this.gainSignal.rampTo(this.initialGain, fadeTime, now() + when);
  }

  public fadeOut(fadeTime: number, when: number = 0) {
    this.gainSignal.cancelAndHoldAtTime(now());
    this.gainSignal.rampTo(0, fadeTime, now() + when);
  }

  dispose() {
    this.panner?.disconnect();
    this.panner?.dispose();

    this.gain.disconnect();
    this.gain.dispose();
  };
};

export default Channel;
