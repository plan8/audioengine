import{ Gain, LFO, Filter, Signal, Reverb, Convolver, CrossFade, now, ToneAudioBuffer } from 'tone';
import { waveshapers } from '../../utils';

export interface IBusSettings {
  name: string;
  parent: string;
  gain?: number;
  effects?: any[];
}

export class Bus {
  private input: Gain;
  private output: Gain;
  private muteGain: Gain;
  private effectInstances: any[];
  private lastEffect: any;
  private isMuted: boolean = false;
  private resolveSources: ()=> Promise<any>;

  public settings: any;

  constructor(settings: IBusSettings, resolveSources: ()=> Promise<any> ) {
    this.resolveSources = resolveSources;
    this.settings = settings;
    this.setupAudioGraph();
  }

  private setupAudioGraph() {
    const initalGain = this.settings.gain !== undefined ? this.settings.gain : 1;
    this.input = new Gain(initalGain);
    this.output = new Gain(1);
    this.muteGain = new Gain(1);
    this.effectInstances = [];
    this.lastEffect = this.input;
    this.setupEffects(this.settings.effects);
  }

  private setupEffects(effects: any) {

    if (effects) {
      for (let i = 0; i < effects.length; i++) {
        const fxSettings = effects[i];
        const effect = this.createEffect(fxSettings);

        this.lastEffect.connect(effect);

        // lastEffect is piped into the new effect and we can set the amount of send
        // this.lastEffect.connect(crossfader);
        // crossfader.connect(effect);

        if (effect.name === 'Convolver') {
          // both go to output
          const outputGain = new Gain(1);
          this.lastEffect.connect(outputGain);
          effect.connect(outputGain);

          this.lastEffect = outputGain;
        }
        else {
          this.lastEffect.connect(effect);
          this.lastEffect = effect;
        }

        this.effectInstances.push(effect);
      }

      this.lastEffect.connect(this.muteGain);
      this.muteGain.connect(this.output);
      this.lastEffect = this.output;
    }
    else {
      this.lastEffect.connect(this.muteGain);
      this.muteGain.connect(this.output);
      this.lastEffect = this.output;
    }
  }

  private createEffect(fxSettings) {
    let fxInstance;

    const { effectType, settings } = fxSettings;
    let frequency;
    let depth;
    let vol;

    switch (effectType) {
      case 'LFOVolume':
        vol = new Gain();
        frequency = settings && settings.frequency;
        depth = settings && settings.depth;
        const lfo = new LFO(frequency, 1 - depth, 1); // hertz, min, max

        if (settings.waveshaper) {
          const { sparseness } = settings.waveshaper;
          const bottomOutShaper = waveshapers.bottomOut(sparseness);

          lfo.connect(bottomOutShaper);
          bottomOutShaper.connect(vol.gain);
        }
        else {
          lfo.connect(vol.gain);
        }

        lfo.start();
        fxInstance = vol;

        break;

      case 'LPF':
        frequency = settings && settings.frequency;

        depth = settings && settings.depth;
        const filter = new Filter(17000, 'lowpass');

        fxInstance = filter;
        break;

      case 'reverb':
        const reverb = new Convolver();
        if ((settings && settings.sound)) {
          this.resolveSources().then((buffers)=>{
            if (buffers.length) {
              reverb.buffer = buffers[0] as ToneAudioBuffer;
            }
          });
        }
        fxInstance = reverb;
        break;

      default:
        fxInstance = new Gain();
        break;
    }

    return fxInstance;
  }

  public connect(destination) {
    this.lastEffect.connect(destination);
  }

  public setVolume(volume: number, fadeTime: number = 1, startTime?: number) {
    this.output.gain.rampTo(volume, fadeTime, startTime);
  }

  public setMuteState(shouldMute: boolean) {
    const fadeTime = 0.2;
    if (shouldMute) {
      this.muteGain.gain.rampTo(0, fadeTime, now());

      return new Promise((resolve) => {
        setTimeout(() => resolve(true), fadeTime);
      });
    }
    else {
      this.muteGain.gain.rampTo(1, fadeTime, now());

      return new Promise((resolve) => {
        setTimeout(() => resolve(false), fadeTime);
      });
    }
  }

  dispose() {
    this.input.dispose();
    this.output.dispose();
    this.muteGain.dispose();
    this.effectInstances.forEach(effect => effect.dispose());
  }
};

export default Bus;
