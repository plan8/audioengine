import { Sampler, ToneAudioBuffer, ToneBufferSource, now , Midi} from 'tone';
import { Channel } from '../../routing/Channel';
import { ISoundSettings } from '../../controllers/SceneManager'; // @scazan this should probably live here instead
import { IInstrument } from '../../instruments';

export default class implements IInstrument {
  public player: Sampler;
  public channel: Channel;
  public name: string;

  constructor(buffers: {[note: number]: ToneAudioBuffer | AudioBuffer } | {}, options: ISoundSettings ) {
    const { name, gain=0, spatial={} } = options;

    const samplerBuffers = buffers;

    const player = new Sampler(samplerBuffers);
    const channel = new Channel(player, options);

    this.name = name;
    this.player = player;
    this.channel = channel;
  }

  setGain(value: number, fadeTime=0, startTime=0) {
    this.channel.setGain(value, fadeTime, startTime);
  }

  connect(destination: any) {
    this.channel.connect(destination);
  }

  getState() {
    return 'started';
  }

  private getActiveSources() {
    // @ts-ignore
    return this.player._activeSources;
  }

  getNumberOfActiveSources() {
    let numberOfActiveSources = 0;
    const activeSources = this.getActiveSources();
    if (activeSources) {
      activeSources.forEach((mapElement: any) => {
        numberOfActiveSources += mapElement.length;
      });
    }
    return numberOfActiveSources;
  }

  stopOldestNote() {
    const notes = [];
    const activeSources = this.getActiveSources();

    if (activeSources) {
      activeSources.forEach((mapElement: any, key: string) => {
        mapElement.forEach((note: ToneBufferSource) => {
          notes.push({key: key, note: note});
        });
      });

      const oldest = notes.reduce(function(prev, curr) {
        return prev.note._stopTime < curr.note._stopTime ? prev : curr;
      });

      this.player.triggerRelease(Midi(oldest.key).toFrequency())
    }
  }

  play({ noReset=true, options }) {
    const { note, velocity } = options;

    //resets channel gain to initial value
    !noReset && this.channel.resetGain();

    //@andreas _activeSources only seem to work when useing triggerAttack (not triggerAttackRelease)
    this.player.triggerAttack(Midi(note).toFrequency(), now(), velocity);
  }

  stop() {
  }

  fadeOut(fadeTime, startTime, onComplete) {
    this.channel.fadeOut(fadeTime, startTime);

    setTimeout(() => {
      onComplete && onComplete(this);
    }, (startTime + fadeTime) * 1000);
  }

  dispose() {
    this.player.dispose();
    this.player.disconnect();
    this.channel.dispose();
  }
};
