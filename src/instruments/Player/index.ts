import { Player, ToneAudioBuffer, now } from 'tone';
import { Channel } from '../../routing/Channel';
import { ISoundSettings } from '../../controllers/SceneManager'; // @scazan this should probably live here instead
import { IInstrument } from '../../instruments';

export default class implements IInstrument {
  public player: Player;
  public channel: Channel;
  public name: string;

  constructor(buffer: ToneAudioBuffer | AudioBuffer, options: ISoundSettings ) {
    const { name, loop=false, loopStart, loopEnd } = options;
    const player = new Player(buffer);
    player.loop = loop;

    if (loopStart) {
      player.loopStart = loopStart;
    }

    if (loopEnd) {
      player.loopStart = loopEnd;
    }

    this.name = name;

    const channel = new Channel(player, options);

    this.player = player;
    this.channel = channel;
  }

  setGain(value: number, fadeTime=0, startTime=0) {
    this.channel.setGain(value, fadeTime, startTime);
  }

  connect(destination: any) {
    this.channel.connect(destination);
  }

  getState() {
    return this.player.state;
  }

  play({ noReset=true, when=0, fadeIn=0.01 }) {
    try {
      // TODO: @scott doesn't support overlapping sounds
      if (this.player.state !== 'started') {
        //resets channel gain to initial value
        !noReset && this.channel.resetGain();
        this.player.start(now() + when);
      }

      this.channel.fadeIn(fadeIn, when);
    }
    catch (err) {
      (window as any).AE_DEBUG && console.warn(err);
    }
  }

  stop(when: number) {
    this.player.stop(when);
  }

  fadeOut(fadeTime, startTime, onComplete) {
    this.channel.fadeOut(fadeTime, startTime);

    setTimeout(() => {
      onComplete && onComplete(this);
    }, (startTime + fadeTime) * 1000);
  }

  dispose() {
    this.player.dispose();
    this.player.disconnect();
    this.channel.dispose();
  }
};
