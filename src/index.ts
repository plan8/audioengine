import SceneManager from './controllers/SceneManager';


// @ts-ignore
window.TONE_SILENCE_LOGGING = true;

export { default as SceneManager } from './controllers/SceneManager';
export { default as Bus } from './routing/Bus';
export { default as Channel } from './routing/Channel';

// utils
export { default as utils } from './utils';
export { default as animationUtils } from './utils/animation';
export { default as createFSM } from './utils/FSM';

export * as Tone from 'tone'; // export all of Tone if needed

export default SceneManager;
