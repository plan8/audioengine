import { WaveShaper, context as toneContext } from 'tone';
export interface IVector2 {
  x: number;
  y: number;
}
export interface IVector3 {
  x: number;
  y: number;
  z: number;
}

export const calculateDistance = (vec1: IVector2, vec2: IVector2) => {
  return Math.sqrt(((vec1.x - vec2.x)**2) + ((vec1.y - vec2.y)**2));
};

export const clip = (value: number, min: number, max: number = 1) => Math.min(Math.max(value, min), max);

export const getRangeBasedOnDistance = (settings: {
  listenerPosition: IVector3,
  position: IVector3,
  maxDistance: number,
  refDistance: number,
}): number => {
  const {
    maxDistance,
    refDistance,
  } = settings;
  const listenerPosition = { x: settings.listenerPosition.x, y: settings.listenerPosition.z };
  const position = { x: settings.position.x, y: settings.position.z };

  const distance = calculateDistance(listenerPosition, position);

  if (distance <= refDistance) {
    return 1;
  }

  const clippedDistance = clip(Math.abs(distance - refDistance), 0, maxDistance);
  const range = -(clippedDistance / (maxDistance - refDistance)) + 1; // scale and invert range

  return distance > maxDistance ? 0 : range;
};

export const waveshapers = {
  bottomOut: (bottomBalance: number = 1) => new WaveShaper((val: number) => clip((val * 2) - bottomBalance, 0, 1), 1024 * 4),
};

export const getContext = () => toneContext;

export const setMuteOnBlur = (busToMute: string, fadeTime: number = 0.3, engineInstance: any) => {
  window.addEventListener('blur', () => {
    engineInstance.setBusVolume(busToMute, 0, fadeTime);
  });

  window.addEventListener('focus', () => {
    engineInstance.setBusVolume(busToMute, 1, fadeTime);
  });
};

// https://developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent
// @ts-ignore
export const isWebkit = (UA: string) => /\b(iPad|iPhone|iPod)\b/.test(UA) && /WebKit/.test(UA) && !/Edge/.test(UA) && !window.MSStream;
export const isMobile = (UA: string) => UA.match(/Mobi/);

export default {
  calculateDistance,
  getRangeBasedOnDistance,
  waveshapers,
  clip,
  getContext,
  setMuteOnBlur,
  isWebkit,
  isMobile,
};

