
export const getFPS = (now: number, previous: number) => 1000 / (now - previous);

export const getNormalizedForFPS = (value: number, fps: number) => value * (120 / fps);

export default {
  getFPS,
  getNormalizedForFPS,
};
