// a simple Finite State Machine

export default (stateMachineDefinition: any) => {
  const machine = {
    value: stateMachineDefinition.initialState,
    transition(currentState, event) {
      const currentStateDefinition = stateMachineDefinition[currentState];
      const destinationTransition = currentStateDefinition?.transitions[event];

      if (!destinationTransition) {
        return false;
      }

      const destinationState = destinationTransition.target;
      const destinationStateDefinition = stateMachineDefinition[destinationState];

      destinationTransition.action && destinationTransition.action(machine.value);
      currentStateDefinition.actions.onExit && currentStateDefinition.actions.onExit(machine.value);
      destinationStateDefinition.actions.onEnter && destinationStateDefinition.actions.onEnter(machine.value);
      machine.value = destinationState;

      return machine.value;
    },
  };

  return machine;
}
