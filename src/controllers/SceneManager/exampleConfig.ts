export default {
    "scenes": {
        "main": {
            "sounds": [
                {
                    "name": "ambient_main",
                    "sound": {
                        "91": "note"
                    }
                },
                {
                    "name": "tree",
                    "sound": "tree",
                    "bus": "soundfx",
                    "loop": true,
                    "gain": 0.15,
                    "spatial": {
                        "positioned": true,
                        "static": true,
                        "refDistance": 25,
                        "rolloffFactor": 12
                    }
                }
            ]
        },
        "otherScene": {
            "sounds": [
                {
                    "name": "ambient_main",
                    "sound": "daytime_ambience"
                }
            ]
        }
    },
    "sources": [
        {
            "name": "daytime_ambience",
            "file": "daytime_ambience",
            "bus": "soundfx",
            "gain": 1,
            "loop": true,
            "spatial": {
                "positioned": false,
                "static": true
            }
        },
        {
            "name": "tree",
            "file": "soul_tree_loop",
            "bus": "soundfx",
            "loop": true
        }
    ],
    "busses": [
        {
            "name": "master",
            "parent": "$OUT"
        },
        {
            "name": "soundfx",
            "parent": "transition"
        }
    ]
}
