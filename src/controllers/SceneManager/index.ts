import { Gain, ToneAudioBuffer, getListener, start, now, optionsFromArguments } from 'tone';
import TonePlayer from '../../instruments/Player';
import Sampler from '../../instruments/Sampler';
import { IInstrument } from '../../instruments';
import Utils, { calculateDistance, IVector3 } from '../../utils';
import Bus, { IBusSettings } from '../../routing/Bus';
import BufferManager, { ISourceSettings } from '../BufferManager';

export type InstrumentType = 'player' | 'sampler';

export interface ISoundSettings extends ISourceSettings {
  sound: string;
  type?: InstrumentType;
  root?: number; // the root note of the sample
  loop?: boolean;
  loopStart?: number;
  loopEnd?: number;
}

export interface IConfig {
  scenes: {
    [key: string]: {
      sounds: Array<ISoundSettings>;
    }
  };
  sources: Array<ISourceSettings>;
  busses: Array<IBusSettings>;
}

export default class {
  private players: Map<string, IInstrument>;
  private scenes: Map<string, any>;
  private masterGain: Gain;
  private soundSources: Map<string, any>;
  private busses: Map<string, any>;
  private bufferManager: BufferManager;
  private currentScene: any;
  private listener: any;

  public audioIsInitialized: boolean = false;
  public masterContext: AudioContext;
  public utils = Utils; // expose utils to the public
  public loadingState: 'initializing' | 'loading' | 'loaded' = 'initializing';

  constructor(config: IConfig) {
    (window as any).AE_DEBUG && console.log('AudioEngine Loaded');
    const { scenes, sources, busses } = config;

    // store scenes
    this.scenes = new Map();
    Object.entries(scenes)
      .forEach( ([sceneName, scene]: [string, any]) => {
        scene.name = sceneName;
        this.scenes.set(scene.name, scene);
      });

    // store sound references so we can override
    this.soundSources = new Map();
    sources
      .forEach( (sound: ISoundSettings) => {
        this.soundSources.set(sound.name, sound);
      });

    this.bufferManager = new BufferManager(sources, '/audio/');

    this.busses = new Map();

    this.setupAudioGraph(busses);
    this.listener = getListener();
  }

  private setupAudioGraph(busses: IBusSettings[]) {
    this.players = new Map();

    this.masterGain = new Gain(1).toDestination();

    //create busses
    busses.forEach( (busSettings: IBusSettings) => {
      const sourcesToLoad = [];
      if (busSettings.effects) {
        busSettings.effects.map((effect)=>{
          if (effect.settings?.sound) {
            sourcesToLoad.push(effect.settings.sound);
          }
        })
      }

      const bus = new Bus(busSettings, ()=>{
        return new Promise((resolve, reject)=>{
          if (sourcesToLoad.length > 0) {
            Promise.all(
              sourcesToLoad.map( (source)=> this.bufferManager.getSoundBuffer(source) )
            ).then((buffers)=>{
              resolve(buffers)
            });
          }else {
            resolve(true);
          }
        })
      });
      this.busses.set(busSettings.name, bus);
    });

    //connect busses
    this.busses.forEach((bus)=>{
      const destination = bus.settings.parent === '$OUT' ? this.masterGain : this.busses.get(bus.settings.parent).input;
      bus.connect(destination)
    })
  }

  public initAudio() {
    return start()
      .then(() => {
        this.audioIsInitialized = true;
      });
  }

  public destroy() {
   this.players.forEach((player: IInstrument) => player.dispose());

   this.busses.forEach((bus) => bus.dispose());
  }

  public async loadScene(sceneName: string, setCurrent: boolean = false, onLoaded?: any, fadeTime?: number) {
    let scene = this.scenes.get(sceneName);
    if (!scene) {
      scene = this.scenes.get('main');
    }

    if (setCurrent) {
      this.currentScene = scene;
      this.loadingState = 'loading';
    }

    const loadPromises = scene.sounds
      .map((sound: any) => this.createSoundPlayer(scene, sound, fadeTime));

    return await Promise.all(loadPromises)
      .then(() => {
        (window as any).AE_DEBUG && console.log('Audio: loaded scene');
        this.loadingState = 'loaded';
        onLoaded && onLoaded();
      });
  }

  private getPlayerKeysForScene(sceneName: string): string[] {
    const playerKeys = [];
    this.players.forEach((_, key: string) => {
      if (key.match(sceneName + ':')) {
        playerKeys.push(key);
      }
    });

    return playerKeys;
  }

  public changeScene(sceneName: string, settings: { fadeOutTime?: number, fadeInTime?: number, onLoaded?: any }) {
    // TODO: @scott check for overlaps first and don't reload sounds that are already loaded
    if (sceneName === this.currentScene?.name) {
      return false;
    }

    const { onLoaded } = settings;
    const fadeOutTime = settings.fadeOutTime !== null ? settings.fadeOutTime : 0;

    const playerKeys = this.getPlayerKeysForScene(this.currentScene?.name);
    const allPlayersForPreviousScene = playerKeys.map( key => this.players.get(key) );

    Promise.all(allPlayersForPreviousScene
      .map((player: any) =>
        new Promise((resolve) =>
          this.stopPlayer(player, {
            fadeOut: fadeOutTime,
            startTime: 0,
            onComplete: () => resolve(true),
          })
        )
      )
    )
    .then(() => {
      this.unloadSounds(allPlayersForPreviousScene);
    });

    // cleanup this scene from our players Map
    playerKeys.forEach( key => this.players.delete(key) );

    return this.loadScene(sceneName, true, onLoaded, fadeOutTime);
  }

  private unloadSounds(players: any) {
    players.forEach((player: IInstrument) => player.dispose());
  }

  public unloadScene(sceneName: string) {
    const allPlayers = this.players;

    allPlayers.forEach((player: IInstrument, key: string, thisMap) => {
      // only remove sounds for current scene
      if (key.match(sceneName + ':')) {
        player.dispose();
        thisMap.delete(key);
      }
    });
  }

  private async createSoundPlayer(scene: any, sound: ISoundSettings, fadeTime?: number) {
    let soundSource = null;
    const soundSpatial = sound.spatial;

    let buffer = undefined;
    // @andreas @scazan check if we are providing a list of keys for an instrument that takes in multiple buffers
    if (typeof sound.sound === 'object') {
      const allBufferPromises = [];
      buffer = {};

      Object.keys(sound.sound)
        .forEach(( key: string ) => {
          soundSource = this.soundSources.get(sound.sound[key]);

          const bufferPromise = this.bufferManager.getSoundBuffer(sound.sound[key])
            .then((buff: ToneAudioBuffer) => {
              buffer[key] = buff;
            });

          allBufferPromises.push(bufferPromise);
        });

      // @scazan load all buffers in parallel
      await Promise.all(allBufferPromises);
    }
    else {
      soundSource = this.soundSources.get(sound.sound);
      buffer = await this.bufferManager.getSoundBuffer(sound.sound);
    }

    sound = {...soundSource, ...sound}
    sound.spatial = {...soundSource.spatial, ...soundSpatial}

    const InstrumentClass = sound.type === 'sampler' ? Sampler : TonePlayer; // TODO: @scazan polymorphic and needs it's own structure

    const player = new InstrumentClass(buffer, sound);

    if (sound.bus) {
      player.connect(this.busses.get(sound.bus).input);
    }
    else {
      player.connect(this.masterGain);
    }
    if (sound.sends) {
      sound.sends.forEach(send => {
        const bus = this.busses.get(send.bus);
        if (bus) {
          player.channel.addSend(bus, {
            gain: send.gain,
            pre: send.pre
          });
        }
      });
    }
    // TODO: @scott remove into it's own function
    this.players.set(`${ scene.name }:${ sound.name }`, player);

    const isPositioned = sound.spatial?.positioned;
    if (!isPositioned && sound.spatial?.static) {
      this.triggerForScene(sound.name, { noReset: true });
    }

    player.setGain(sound.gain !== undefined ? sound.gain : 1, fadeTime || 0, 0);
  }

  public getBus(key: string) {
    return this.busses.get(key);
  }

  public getInstrument(key: string) {
    return this.players.get(key);
  }

  public getInstrumentForScene(key: string) {
    return this.players.get(`${this.currentScene?.name}:${key}`);
  }

  public trigger(key: string, options?: any) {
    options = options || {};

    const player = this.getInstrument(key);
    player && player.play(options);
  }

  public triggerForScene(key: string, options?: any) {
    if (!this.currentScene) {
      (window as any).AE_DEBUG && console.warn('AudioEngine: No current scene')
      return false;
    }

    this.trigger(`${this.currentScene?.name}:${key}`, options);
  }
  public triggerAllForScene(scene: string, options?: any) {
    const playerKeys = this.getPlayerKeysForScene(scene);
    playerKeys.forEach((key)=>{
      this.trigger(key, options);
    })
  }
  public stopForScene(key: string, options?: any) {
    this.stop(`${this.currentScene?.name}:${key}`, options);
  }

  public stop(key: string, options?: { fadeOut?: number, startTime?: number, onComplete?: any }) {
    options = options || {};

    const { fadeOut=0, startTime=0 } = options;;
    const player = this.players.get(key);

    this.stopPlayer(player, { fadeOut, startTime, onComplete: options?.onComplete });
  }

  private stopPlayer(player: IInstrument, options: { fadeOut?: number, startTime?: number, onComplete?: any } = {}) {
    const {
      startTime,
      fadeOut,
    } = options;

    this.fadeOutPlayer(player, options);
    player.stop(now() + startTime + fadeOut);
  }

  public fadeOut(key: string, options?: { fadeOut?: number, startTime?: number, onComplete?: any }) {
    const player = this.players.get(key);

    this.fadeOutPlayer(player, options);
  }

  public fadeOutPlayer(instrument: IInstrument, options: { fadeOut?: number, startTime?: number, onComplete?: any } = {}) {
    const {
      startTime,
      fadeOut,
      onComplete,
    } = options;

    instrument.fadeOut(fadeOut, startTime, onComplete);
  }

  public setPositionForScene(key: string, position: IVector3) {
    if (!position || !this.currentScene) {
      return true;
    }

    this.setPosition(`${this.currentScene.name}:${key}`, position);
  }

  public setPosition(key: string, position: IVector3) {
    const instrument = this.players.get(key);

    if (instrument == null) {
      return false;
    }

    // TODO: @scazan factor this out. Don't access directly
    const { channel } = instrument;

    channel.setPosition(position.x, position.y, position.z);

    // only trigger if we are within distance
    const distanceToObject = calculateDistance({x: position.x, y: position.z}, {x: this.listener.positionX.value, y: this.listener.positionZ.value});

    const minimumListeningDistance = channel.maxAudibleDistance;
    if (channel.isPositional) {
      if (instrument.getState() === 'stopped' && distanceToObject < minimumListeningDistance) {
        this.trigger(key);
      }
      else if (instrument.getState() != 'stopped' && distanceToObject > minimumListeningDistance) {
        this.stopPlayer(instrument, {
          fadeOut: 0,
          startTime: 0,
        });
      }
    }
  }

  public setListenerPosition(playerPosition: IVector3, playerRotation: IVector3) {
    if (!playerPosition) {
      return true;
    }

    this.listener.set({
      positionX: playerPosition.x,
      positionY: playerPosition.y,
      positionZ: playerPosition.z,

      forwardX: Math.sin(-playerRotation.y),
      forwardY: 0,
      forwardZ: -(Math.cos(-playerRotation.y)),
    });
  }

  public setBusVolume(key: string, volume: number, fadeTime: number = 1, startTime?: number) {
    startTime = startTime || now();
    const bus = this.busses.get(key);

    if (key === '$OUT') {
      this.masterGain.gain.rampTo(volume, fadeTime, startTime);
    }
    else {
      bus?.setVolume(volume, fadeTime, startTime);
    }
  }

  public setBusMute(key: string, shouldMute: boolean) {
    if (key === '$OUT') {
      this.masterGain.gain.value = shouldMute ? 0 : 1;
      return Promise.resolve(true);
    }

    return this.busses.get(key)?.setMuteState(shouldMute);
  }

  public setBaseFilePath(path: string) {
    this.bufferManager.setBaseFilePath(path);
  }
  public now() {
    return now()
  }
};

