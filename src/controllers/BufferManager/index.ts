import{ ToneAudioBuffer } from 'tone';

export interface ISourceSettings {
  name: string;
  file: string;
  bus?: string;
  loop?: boolean;
  gain?: number;
  sends?: [
    {
      bus: string;
      gain: number;
      pre: boolean;
    }
  ];
  spatial?: {
    positioned?: boolean;
    static?: boolean;
    refDistance?: number;
    rolloffFactor?: number;
  }
}

export default class {
  private sources: Map<string, any>;
  private buffers: Map<string, ToneAudioBuffer>;
  private baseFilePath: string;

  constructor(sources: ISourceSettings[], baseFilePath: string) {
    this.baseFilePath = baseFilePath;

    // store sound references
    this.sources = new Map();
    sources
      .forEach( (sound: ISourceSettings) => {
        this.sources.set(sound.name, sound);
      });

    this.buffers = new Map();
  }

  async getSoundBuffer(name: string): Promise<ToneAudioBuffer|AudioBuffer> {
    const existingBuffer = this.buffers.get(name);

    if (existingBuffer) {
      return Promise.resolve(existingBuffer);
    }

    const sound = this.sources.get(name);
    if (!sound.file) {
      (window as any).AE_DEBUG && console.warn('Sound:', name, 'is not defined in the sound list.')
    }

    return ToneAudioBuffer.load(`${this.baseFilePath}${sound.file}.[ogg|mp3]`);
  }

  public setBaseFilePath(path: string) {
    // TODO: @scott remove trailing slash if any
    this.baseFilePath = path;
  }
};

