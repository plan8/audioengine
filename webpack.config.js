const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env) => {
  return {
    entry: './src/index.ts',
    mode: env.production ? 'production' : 'development',
    watch: env.production ? false : true,
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      alias: {
        utils: path.resolve(__dirname, 'src/utils/'),
        classes: path.resolve(__dirname, 'src/classes/'),
      },

    },
    output: {
      filename: 'AudioEngine.js',
      path: path.resolve(__dirname, 'dist'),
      library: {
        name: 'AudioEngine',
        type: 'umd',
      },
    },

    devtool: env.production ? undefined : 'inline-source-map',
    optimization: {
      minimize: !!env.production,
      minimizer: [new TerserPlugin({
        terserOptions: {
          compress: {
            // module: true,
            drop_console: true,
            keep_fargs: false,
            passes: 3,
            pure_getters: true,
            unsafe_methods: true,
            unsafe_proto: true,
          },
        }
      })],
    },
  };
};
